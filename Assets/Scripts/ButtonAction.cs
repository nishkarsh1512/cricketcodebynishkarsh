using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonAction : MonoBehaviour
{
    Swing swing;

    Spin spin;

    BallPos ballPos;

    public void OnButtonPress()
    {
        ballPos = FindObjectOfType<BallPos>();

        Vector3 ballPosPosition = ballPos.transform.position;
        swing = FindObjectOfType<Swing>();
        Rigidbody swingRigidbody = swing.GetComponent<Rigidbody>();
        Vector3 newPosition = ballPosPosition; // Replace x, y, and z with your desired coordinates
        swingRigidbody.isKinematic = true;
        swing.enabled = false;
        swing.transform.position = newPosition;
    }

    public void OnButtonPress(string buttonName)
    {
        if (buttonName == "direction")
        {
            ballPos = FindObjectOfType<BallPos>();
            swing = FindObjectOfType<Swing>();
            Vector3 newPosition = new Vector3(-0.921f, 2f, -6.63f); // Replace x, y, and z with your desired coordinates

            // Change the position of the BallPos object
            ballPos.transform.position = newPosition;
            swing.transform.position = newPosition;
        }
        else
        {
            swing = FindObjectOfType<Swing>();
            Rigidbody swingRigidbody = swing.GetComponent<Rigidbody>();

            swingRigidbody.isKinematic = false;
            swing.enabled = true;
            swing.ResetState();
        }
    }

    public void OnButtonPress(float buttonNos)
    {
        spin = FindObjectOfType<Spin>();
        Rigidbody spinRigidbody = spin.GetComponent<Rigidbody>();

        spinRigidbody.isKinematic = false;
        spin.enabled = true;
        spin.ResetState();
    }
}
