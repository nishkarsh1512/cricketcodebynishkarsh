using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour
{
    public Slider slider;

    public float sharedValue; // Public variable to store the value

    private void Start()
    {
        // Ensure the slider reference is set
        if (slider == null)
        {
            slider = GetComponent<Slider>();
        }

        // Add a listener to the Slider's onValueChanged event
        slider.onValueChanged.AddListener (OnSliderValueChanged);
    }

    public void OnSliderValueChanged(float value)
    {
        // This method is called whenever the Slider's value changes
        sharedValue = value;

        // You can respond to value changes here in real-time
        // For example, update a variable, move an object, or trigger an action
    }
}
