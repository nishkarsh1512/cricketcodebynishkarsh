using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public Transform startPoint;

    public Transform endPoint;

    // public Transform controlPoint;
    private float t = 0.0f; // Parameter value between 0 and 1

    private float speed = 0.5f; // Adjust the speed as needed

    private Rigidbody rb;

    public float duration = 1.0f;

    SliderController sliderController;

    private bool isAnimating = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        enabled = false;
    }

    public void ResetState()
    {
        // Reset any relevant state or variables to their initial values.
        t = 0.0f;
        isAnimating = true;
        rb.isKinematic = true;
        // Additional reset actions, if necessary
    }

    private void Update()
    {
        sliderController = FindObjectOfType<SliderController>();
        float value = sliderController.sharedValue;
        value /= 5;

        rb = GetComponent<Rigidbody>();

        if (isAnimating)
        {
            t += Time.deltaTime / duration;
            if (t >= 1f)
            {
                t = 1.0f;
                isAnimating = false;
                rb.isKinematic = false;
                Vector3 direction =
                    (endPoint.position - transform.position).normalized;
                direction += new Vector3(value, 0, 0);

                // Normalize the direction vector again (optional)
                direction.Normalize();

                // Apply a force in that direction
                rb.AddForce(direction * 10f, ForceMode.Impulse);
            }

            Vector3 p = Vector3.Lerp(startPoint.position, endPoint.position, t);
            transform.position = p;
        }
    }
}
