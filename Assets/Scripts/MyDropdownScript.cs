using UnityEngine;
using UnityEngine.UI;

public class MyDropdownScript : MonoBehaviour
{
    [SerializeField]
    public Dropdown dropdown; // Reference to the Dropdown component

    public float value;

    private void Start()
    {
        // Set the selected option (0 is the index of the first option)
        // Add a listener for the OnValueChanged event
        dropdown.onValueChanged.AddListener (OnDropdownValueChanged);
    }

    public void OnDropdownValueChanged(int index)
    {
        // Handle the dropdown value change here
        float floatValue = (float) index;
        value = floatValue;

        //spin == 1
        //swing == 0
    }
}
