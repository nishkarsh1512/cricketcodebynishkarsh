using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Start is called before the first frame update
    //Scripts for controlling the direction
    [SerializeField]
    float speed = 10f;

    [SerializeField]
    float xRange = 1.7f; //range for x

    // Update is called once per frame
    void Update()
    {
        float xValue = Input.GetAxis("Horizontal") * speed * Time.deltaTime; // Response to horizontal keys
        float zValue = Input.GetAxis("Vertical") * speed * Time.deltaTime; // Response to vertical keys

        Vector3 newPosition =
            transform.position + new Vector3(xValue, 0, zValue);

        // Define your X and Z bounds
        float minX = -2.1f; // Minimum X value
        float maxX = 2.1f; // Maximum X value
        float minZ = 0.0f; // Minimum Z value
        float maxZ = 2.8f; // Maximum Z value

        // Clamp the X and Z positions within the defined bounds
        newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
        newPosition.z = Mathf.Clamp(newPosition.z, minZ, maxZ);

        // Apply the clamped position to the object's transform
        transform.position = newPosition;
    }
}
