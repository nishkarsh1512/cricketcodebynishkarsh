using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swing : MonoBehaviour
{
    public Transform startPoint;

    public Transform endPoint;

    public Transform controlPoint1;

    public Transform controlPoint2;

    public float duration = 1.0f;

    private float t = 0.0f;

    private bool isAnimating = true;

    private Rigidbody rb;

    SliderController sliderController;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        enabled = false;
    }

    public void ResetState()
    {
        // Reset any relevant state or variables to their initial values.
        t = 0.0f;
        isAnimating = true;
        rb.isKinematic = true;
        // Additional reset actions, if necessary
    }

    void Update()
    {
        sliderController = FindObjectOfType<SliderController>();
        float value = sliderController.sharedValue;
        value /= 1000;

        if (isAnimating)
        {
            t += Time.deltaTime / duration;

            if (t >= 1.0f)
            {
                t = 1.0f;
                isAnimating = false;
                rb.isKinematic = false;

                // Calculate the direction from the current position to the end point
                Vector3 direction =
                    (endPoint.position - transform.position).normalized;

                // Apply a force in that direction
                rb.AddForce(direction * 10f, ForceMode.Impulse);
            }

            if (value != 0)
            {
                controlPoint1.position =
                    new Vector3(controlPoint1.position.x + value,
                        controlPoint1.position.y,
                        controlPoint1.position.z);
                controlPoint2.position =
                    new Vector3(controlPoint2.position.x + value,
                        controlPoint2.position.y,
                        controlPoint2.position.z);
                Vector3 p =
                    CubicBezier(startPoint.position,
                    controlPoint1.position,
                    controlPoint2.position,
                    endPoint.position,
                    t);

                transform.position = p;
            }
            else
            {
                Vector3 lineMidPoint =
                    (startPoint.position + endPoint.position) / 2f;

                // Set controlPoint1 and controlPoint2 to be on this line
                controlPoint1.position = lineMidPoint;
                controlPoint2.position = lineMidPoint;
                Vector3 p =
                    CubicBezier(startPoint.position,
                    controlPoint1.position,
                    controlPoint2.position,
                    endPoint.position,
                    t);

                transform.position = p;
            }
        }
    }

    // Cubic Bezier interpolation function
    private Vector3
    CubicBezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
}
